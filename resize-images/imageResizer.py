import PIL
import os
import os.path
from PIL import Image

f = "../trainers/Tensorflow/workspace/images/train"
for file in os.listdir(f):
    f_img = f+"/"+file
    img = Image.open(f+"/"+file)
    img = img.resize((320,320))
    img = img.rotate(-90)
    img.save(f_img)