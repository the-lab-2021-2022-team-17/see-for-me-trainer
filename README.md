# See-for-me-Trainer

# The lab 2021-2022

## Teamname: Team 17

### Team members

| Name | Email | Role |
| ------ | ------ | ------ |
| Jari Moons | jari.moons@student.kdg.be | IFS |
| Thomas Vandewalle | thomas.vandewalle@student.kdg.be | IFS |
| Pieter-Jan Livens | pieterjan.livens@student.kdg.be | IFS |

## Description
This project is responsible for training the model to recognize and detect cars and bicycles.


## Setup
First run download the packages found in requirements.txt. Afterwards run the setup script. If no errors occured, you will be able to start training your model.
