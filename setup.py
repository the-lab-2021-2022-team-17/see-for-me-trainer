import os

import wget

CUSTOM_MODEL_NAME = 'my_ssd_mobnet'

paths = {
    'WORKSPACE_PATH': os.path.join('trainers', 'Tensorflow', 'workspace'),
    'SCRIPTS_PATH': os.path.join('trainers', 'Tensorflow', 'scripts'),
    'APIMODEL_PATH': os.path.join('trainers', 'Tensorflow', 'models'),
    'ANNOTATION_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'annotations'),
    'IMAGE_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'images'),
    'MODEL_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models'),
    'PRETRAINED_MODEL_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'pre-trained-models'),
    'CHECKPOINT_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models', CUSTOM_MODEL_NAME),
    'EVAL_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models', CUSTOM_MODEL_NAME, "eval"),
    'OUTPUT_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models', CUSTOM_MODEL_NAME, 'export'),
    'TFJS_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models', CUSTOM_MODEL_NAME, 'tfjsexport'),
    'TFLITE_PATH': os.path.join('trainers', 'Tensorflow', 'workspace', 'models', CUSTOM_MODEL_NAME, 'tfliteexport'),
    'PROTOC_PATH': os.path.join('trainers', 'Tensorflow', 'protoc')
}

os.system("git clone https://github.com/tensorflow/models " + paths['APIMODEL_PATH'])

# Install Tensorflow Object Detection
print("Clone Tensorflow git")
url = "https://github.com/protocolbuffers/protobuf/releases/download/v3.15.6/protoc-3.15.6-win64.zip"
wget.download(url)
os.system("move protoc-3.15.6-win64.zip " + paths['PROTOC_PATH'])
os.system("cd " + paths['PROTOC_PATH'] + " && tar -xf protoc-3.15.6-win64.zip")
os.environ['PATH'] += os.pathsep + os.path.abspath(os.path.join(paths['PROTOC_PATH'], 'bin'))
os.system("cd trainers/Tensorflow/models/research && protoc object_detection/protos/*.proto --python_out=. && "
          "copy object_detection\\packages\\tf2\\setup.py setup.py && python setup.py build && python setup.py install")
os.system("cd trainers/Tensorflow/models/research/slim && pip install -e .")
os.system("rmdir /s /q " + paths["APIMODEL_PATH"] + "\\.git")
os.system("rmdir /s /q " + paths["APIMODEL_PATH"] + "\\.github")

print("Clone TF records generator")
os.system("git clone https://github.com/nicknochnack/GenerateTFRecord " + paths['SCRIPTS_PATH'])
os.system("rmdir /s /q " + paths['SCRIPTS_PATH'] + "\\.git")

print("Update and install packages")
os.system("python -m pip install --user --use-feature=2020-resolver " + os.path.join("trainers", "Tensorflow", "models",
                                                                                     "research"))
os.system("pip install --upgrade pip")
os.system("pip install -r .\\requirements.txt")

