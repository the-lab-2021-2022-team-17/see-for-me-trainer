import os
import tensorflow as tf
from object_detection.protos import pipeline_pb2
from google.protobuf import text_format
import wget


class ObjectDetectionTrainer:
    __LABELS = [{'name': 'Car', 'id': 1}, {'name': 'Bicycle', 'id': 2}]
    __CUSTOM_MODEL_NAME = 'my_ssd_mobnet'
    __PRETRAINED_MODEL_NAME = 'ssd_mobilenet_v2_fpnlite_320x320_coco17_tpu-8'
    __TF_RECORD_SCRIPT_NAME = 'generate_tfrecord.py'
    __LABEL_MAP_NAME = 'label_map.pbtxt'

    __PATHS = {
        'WORKSPACE_PATH': os.path.join('Tensorflow', 'workspace'),
        'SCRIPTS_PATH': os.path.join('Tensorflow', 'scripts'),
        'APIMODEL_PATH': os.path.join('Tensorflow', 'models'),
        'ANNOTATION_PATH': os.path.join('Tensorflow', 'workspace', 'annotations'),
        'IMAGE_PATH': os.path.join('Tensorflow', 'workspace', 'images'),
        'MODEL_PATH': os.path.join('Tensorflow', 'workspace', 'models'),
        'PRETRAINED_MODEL_PATH': os.path.join('Tensorflow', 'workspace', 'pre-trained-models'),
        'CHECKPOINT_PATH': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME),
        'EVAL_PATH': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, "eval"),
        'OUTPUT_PATH': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, 'export'),
        'SAVED_MODEL': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, 'export', 'saved_model'),
        'TFJS_PATH': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, 'tfjsexport'),
        'TFLITE_PATH': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, 'tfliteexport'),
        'PROTOC_PATH': os.path.join('Tensorflow', 'protoc')
    }

    __TRAINING_SCRIPT = os.path.join(__PATHS['APIMODEL_PATH'], 'research', 'object_detection', 'model_main_tf2.py')
    __FREEZE_SCRIPT = os.path.join(__PATHS['APIMODEL_PATH'], 'research', 'object_detection', 'exporter_main_v2.py ')
    __TFLITE_SCRIPT = os.path.join(__PATHS['APIMODEL_PATH'], 'research', 'object_detection',
                                   'export_tflite_graph_tf2.py ')
    __FROZEN_TFLITE_PATH = os.path.join(__PATHS['TFLITE_PATH'], 'saved_model')
    __TFLITE_MODEL = os.path.join(__PATHS['TFLITE_PATH'], 'saved_model', 'detect.tflite')

    __FILES = {
        'PIPELINE_CONFIG': os.path.join('Tensorflow', 'workspace', 'models', __CUSTOM_MODEL_NAME, 'pipeline.config'),
        'TF_RECORD_SCRIPT': os.path.join(__PATHS['SCRIPTS_PATH'], __TF_RECORD_SCRIPT_NAME),
        'TRAIN_SCRIPT': os.path.join(__PATHS['SCRIPTS_PATH'], "model_main_tf2.py"),
        'LABELMAP': os.path.join(__PATHS['ANNOTATION_PATH'], __LABEL_MAP_NAME)
    }

    def __init__(self):
        self.__verifyAbleToTrain()
        self.__downloadPretrainedModel()
        self.__initLabels()
        self.__createTFRecord()
        self.__configurePipeline()

    # use tensorboard for a better representation of the training and evaluation
    # tensorboard --logdir=.
    def runModel(self, timesRunning: int):
        i = len(os.listdir(self.__PATHS['EVAL_PATH']))
        while i < timesRunning:
            self.__trainModel(i)
            self.__evaluateModel()
            i += 1

    def exportModels(self):
        self.__exportCheckpoint()
        self.__exportTflite()

    def __verifyAbleToTrain(self):
        print("Check if models can be trained")
        VERIFICATION_SCRIPT = os.path.join(self.__PATHS['APIMODEL_PATH'], 'research', 'object_detection', 'builders',
                                           'model_builder_tf2_test.py')
        # Verify Installation
        os.system("python " + VERIFICATION_SCRIPT)

    def __downloadPretrainedModel(self):
        print("Download pretrained model")
        os.system("move " + self.__PRETRAINED_MODEL_NAME + '.tar.gz ' + self.__PATHS['PRETRAINED_MODEL_PATH'])
        os.system(
            "cd " + self.__PATHS['PRETRAINED_MODEL_PATH'] + " && tar -zxvf " + self.__PRETRAINED_MODEL_NAME + '.tar.gz')

    def __initLabels(self):
        print("Init labels")
        with open(self.__FILES['LABELMAP'], 'w') as f:
            for label in self.__LABELS:
                f.write('item { \n')
                f.write('\tname:\'{}\'\n'.format(label['name']))
                f.write('\tid:{}\n'.format(label['id']))
                f.write('}\n')

    def __createTFRecord(self):
        print("Create TF records")
        os.system(
            "python " + self.__FILES['TF_RECORD_SCRIPT'] + " -x " + os.path.join(self.__PATHS['IMAGE_PATH'],
                                                                                 'train') + " -l " +
            self.__FILES[
                'LABELMAP'] + " -o " + os.path.join(self.__PATHS['ANNOTATION_PATH'], 'train.record'))
        os.system(
            "python " + self.__FILES['TF_RECORD_SCRIPT'] + " -x " + os.path.join(self.__PATHS['IMAGE_PATH'],
                                                                                 'test') + " -l " + self.__FILES[
                'LABELMAP'] + " -o " + os.path.join(self.__PATHS['ANNOTATION_PATH'], 'test.record'))

    def __configurePipeline(self):
        print("Configure pipeline")
        pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()
        with tf.io.gfile.GFile(self.__FILES['PIPELINE_CONFIG'], "r") as f:
            proto_str = f.read()
            text_format.Merge(proto_str, pipeline_config)

        pipeline_config.model.ssd.num_classes = len(self.__LABELS)
        pipeline_config.train_config.batch_size = 8
        pipeline_config.train_config.fine_tune_checkpoint = os.path.join(self.__PATHS['PRETRAINED_MODEL_PATH'],
                                                                         self.__PRETRAINED_MODEL_NAME,
                                                                         'checkpoint', 'ckpt-0')
        pipeline_config.train_config.fine_tune_checkpoint_type = "detection"
        pipeline_config.train_config.optimizer.momentum_optimizer.learning_rate.cosine_decay_learning_rate.learning_rate_base = 0.0001
        pipeline_config.train_config.optimizer.momentum_optimizer.learning_rate.cosine_decay_learning_rate.warmup_learning_rate = 0.0001
        pipeline_config.train_input_reader.label_map_path = self.__FILES['LABELMAP']
        pipeline_config.train_input_reader.tf_record_input_reader.input_path[:] = [
            os.path.join(self.__PATHS['ANNOTATION_PATH'], 'train.record')]
        pipeline_config.eval_input_reader[0].label_map_path = self.__FILES['LABELMAP']
        pipeline_config.eval_input_reader[0].tf_record_input_reader.input_path[:] = [
            os.path.join(self.__PATHS['ANNOTATION_PATH'], 'test.record')]

        config_text = text_format.MessageToString(pipeline_config)
        with tf.io.gfile.GFile(self.__FILES['PIPELINE_CONFIG'], "wb") as f:
            f.write(config_text)

    def __trainModel(self, i):
        print("Train model")
        command = "python {} --model_dir={} --pipeline_config_path={} --num_train_steps={}".format(
            self.__TRAINING_SCRIPT,
            self.__PATHS['CHECKPOINT_PATH'],
            self.__FILES['PIPELINE_CONFIG'],
            1000 * i)
        os.system(command)

    def __evaluateModel(self):
        print("Evaluate model")
        command = "python {} --model_dir={} --pipeline_config_path={} --checkpoint_dir={} --eval_timeout={}".format(
            self.__TRAINING_SCRIPT,
            self.__PATHS['CHECKPOINT_PATH'],
            self.__FILES['PIPELINE_CONFIG'],
            self.__PATHS['CHECKPOINT_PATH'],
            0)
        os.system(command)

    def __exportCheckpoint(self):
        print("Export pd file and checkpoints")
        command = "python {} --input_type=image_tensor --pipeline_config_path={} --trained_checkpoint_dir={} --output_directory={}".format(
            self.__FREEZE_SCRIPT, self.__FILES['PIPELINE_CONFIG'], self.__PATHS['CHECKPOINT_PATH'],
            self.__PATHS['OUTPUT_PATH'])
        os.system(command)

        # load the saved_model using low-level API
        m = tf.saved_model.load(self.__PATHS['SAVED_MODEL'])

        from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2

        tfm = tf.function(lambda x: m(x))  # full model
        tfm = tfm.get_concrete_function(tf.TensorSpec(m.signatures['serving_default'].inputs[0].shape.as_list(),
                                                      m.signatures['serving_default'].inputs[0].dtype.name))
        frozen_func = convert_variables_to_constants_v2(tfm)
        tf.io.write_graph(graph_or_graph_def=frozen_func.graph, logdir="./",
                          name=self.__PATHS['SAVED_MODEL'] + "/frozen_graph.pb",
                          as_text=False)

    def __exportTflite(self):
        print("Export tflite file")
        command = "python {} --pipeline_config_path={} --trained_checkpoint_dir={} --output_directory={}".format(
            self.__TFLITE_SCRIPT,
            self.__FILES['PIPELINE_CONFIG'],
            self.__PATHS['CHECKPOINT_PATH'],
            self.__PATHS['TFLITE_PATH'])
        os.system(command)

        command = "tflite_convert \
          --saved_model_dir={} \
          --output_file={} \
          --input_shapes=1,300,300,3 \
          --input_arrays=normalized_input_image_tensor \
          --output_arrays='TFLite_Detection_PostProcess','TFLite_Detection_PostProcess:1','TFLite_Detection_PostProcess:2','TFLite_Detection_PostProcess:3' \
          --inference_type=FLOAT \
           --allow_custom_ops".format(self.__FROZEN_TFLITE_PATH, self.__TFLITE_MODEL, )
        os.system(command)


if __name__ == "__main__":
    odt = ObjectDetectionTrainer()
    odt.runModel(55)
    odt.exportModels()
