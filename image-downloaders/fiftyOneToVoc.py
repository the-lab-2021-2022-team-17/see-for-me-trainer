import glob
import os
import shutil
import csv

data_path = "C:/Users/thoma/fiftyone/open-images-v6/train/data"
detection_path = "C:/Users/thoma/fiftyone/open-images-v6/train/labels/detections.csv"
detection2_path = "C:/Users/thoma/fiftyone/open-images-v6/train/labels/detections2.csv"

with open(detection_path, mode='r') as csv_file:
    with open(detection2_path, mode='w', newline='') as detection2_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        fieldnames = ['ImageID', 'Source', 'LabelName', 'Confidence', 'XMin', 'XMax', 'YMin', 'YMax', 'IsOccluded',
                      'IsTruncated', 'IsGroupOf', 'IsDepiction', 'IsInside', 'XClick1X', 'XClick2X', 'XClick3X',
                      'XClick4X', 'XClick1Y', 'XClick2Y', 'XClick3Y', 'XClick4Y']
        detection2_writer = csv.DictWriter(detection2_file, fieldnames=fieldnames)
        for row in csv_reader:
            if line_count != 0 and (row["LabelName"] == "/m/0k4j" or row["LabelName"] == "/m/0199g"):
                print(row)
                detection2_writer.writerow(row)
            line_count += 1

os.system("oidv6-to-voc " + detection2_path + " "
          "-d C:/Users/thoma/fiftyone/open-images-v6/train/metadata/classes.csv "
          "--imgd " + data_path + " "
                                  "--outd ./temp_xml")

for file in glob.glob("./temp_xml/*"):
    xml_file = file.split("\\")[1]
    filename = xml_file.split(".")[0]
    try:
        shutil.move(data_path + "/" + filename + ".jpg", "../trainers/Tensorflow/workspace/images/train/" + filename + ".jpg")
        shutil.move("./temp_xml/" + xml_file, "../trainers/Tensorflow/workspace/images/train/" + xml_file)
    except:
        print(file)

shutil.rmtree('./temp_xml')
