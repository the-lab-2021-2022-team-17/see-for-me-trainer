import fiftyone.zoo as foz

train_data = foz.load_zoo_dataset(
              "open-images-v6",
              split="train",
              label_types=["detections"],
              classes=["Car"],
              max_samples=20000,
          )