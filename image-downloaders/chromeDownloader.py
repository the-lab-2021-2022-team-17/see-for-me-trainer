# from simple_image_download import simple_image_download as simp
#
# search_queries = [
#     'normal cars',
#     'bicycle on the street',
# ]
#
# response = simp.simple_image_download
#
# response().download('normal cars,bicycle on the street', 102)
# First Section: Importing Libraries
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.service import Service
import requests
import urllib.request
import time
import sys
import os

# taking user input
print("What do you want to download?")
download = 'rijdende auto oversteekplaats'
site = 'https://www.google.com/search?tbm=isch&q=' + download

# providing driver path
s=Service("C:\\Users\\thoma\\Documents\\chromedriver\\chromedriver.exe")
driver = webdriver.Chrome(service=s)

# passing site url
driver.get(site)

# if you just want to download 10-15 images then skip the while loop and just write
# driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")


# below while loop scrolls the webpage 7 times(if available)

i = 4

while i < 15:
    # for scrolling page
    driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")

    try:
        # for clicking show more results button
        driver.find_element("/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div/div[5]/input").click()
    except Exception as e:
        pass
    time.sleep(5)
    i += 1

# parsing
soup = BeautifulSoup(driver.page_source, 'html.parser')

# closing web browser
driver.close()

# scraping image urls with the help of image tag and class used for images
img_tags = soup.find_all("img", class_="rg_i")

count = 0
for i in img_tags:
    # print(i['src'])
    try:
        # passing image urls one by one and downloading
        urllib.request.urlretrieve(i['src']+"/test", str(count) + ".jpg")
        count += 1
        print("Number of images downloaded = " + str(count), end='\r')
    except Exception as e:
        pass